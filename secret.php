﻿<?php
include('vendor/autoload.php');

use PHP\Math\BigNumber\BigNumber;

class DH
{
	private $a;
	private $p;
	private $private_key;
	private $mod_key_klient;
	public $mod_key;
	public $secret_key;
	
	function __construct($a, $p)
	{
		$this->a = new BigNumber($a);
		$this->p = new BigNumber($p);
		$this->private_key = new BigNumber($this->BigRandomNumber('100000000000000000000000000000000000000000000', '99999999999999999999999999999999999999999999999'));
		$this->mod_key = $this->fastpow($this->a, $this->private_key, $this->p);
	}
	
	public function generateSecret($mod)
	{
		$mod = new BigNumber($mod);
		$this->secret_key = $this->fastpow($mod, $this->private_key, $this->p);
	}
	
	
	private function fastpow($a, $n, $m)
	{
		$n = new BigNumber($n->getValue());
		$a = new BigNumber($a->getValue());
		$m = new BigNumber($m->getValue());
		while($n != '1')
		{
			$mod = new BigNumber($n);
			if($mod->mod('2') != '0')
			{
				$n = $n->subtract(1);
				$a = $a->multiply($a->getValue());
				$a = $a->mod($m->getValue());
			}
			else 
			{
				$n->divide(2);
				$a->multiply($a);
				$a->mod($m);
			}
		}
		return $a;
	} 
	
	private function BigRandomNumber($min, $max) 
	{
		$difference   = bcadd(bcsub($max,$min),1);
		$rand_percent = bcdiv(mt_rand(), mt_getrandmax(), 47); // 0 - 1.0
		return bcadd($min, bcmul($difference, $rand_percent, 47), 0);
	}
}

if(isset($_POST['a']))
{
	$a = $_POST['a'];
	$p = $_POST['p'];
	$mod = $_POST['mod'];
	
	
	$dh = new DH($a, $p);
	$dh->generateSecret($mod);
	$arr = [];
	$arr['mod'] = $dh->mod_key->getValue();
	$arr['secret'] = $dh->secret_key->getValue();
	$arr = json_encode($arr);
	echo $arr;
}
/*
	$a = new BigNumber('20352011489202798264199162904107873991457957346');
	$p = new BigNumber('54173462027982699162904107873991457903520114892');
	
	$dh = new DH($a, $p);
	echo $dh->mod_key;
*/
	
	
	
	
/*	$rand = BigRandomNumber('100000000000000000000000000000000000000000000', '99999999999999999999999999999999999999999999999');
	
	$priv = new BigNumber($rand);
	
	
	echo $rand . '<br>';
	
	$mod1 = new BigNumber('2094331442414323825982796043600276328141388680');
	
	
	
	$secret = fastpow($mod1, $priv, $p, $co);
	
	echo $secret;
	
	
	
	function fastpow($a, $n, $m, &$co)
	{
		while($n != '1')
		{
			$mod = new BigNumber($n);
			if($mod->mod('2') != '0')
			{
				//echo $n . '<br>';
				$n = $n->subtract(1);
				$a = $a->multiply($a->getValue());
				$a = $a->mod($m->getValue());
				$co++;
			}
			else 
			{
				$n->divide(2);
				$a->multiply($a);
				$a->mod($m);
			}
		}
		return $a;
	} 
	
	function BigRandomNumber($min, $max) {
		$difference   = bcadd(bcsub($max,$min),1);
		$rand_percent = bcdiv(mt_rand(), mt_getrandmax(), 47); // 0 - 1.0
		return bcadd($min, bcmul($difference, $rand_percent, 47), 0);
	}*/
?>