﻿var rng = new SecureRandom();
var sch = 0;
function fastpow(a, n, m)
{	
	sch = 0; 
	while(n > 1)
	{
		
		if (n.mod(2)=='0') {
			n=n.divide('2');
			a=a.multiply(a);
			a = a.mod(m);
			sch++;
		}
		else
		{
			console.log(n.toString());
			n=n.minus(1);
			a=a.multiply(a);
			a = a.mod(m);
			
		}
		
		
	}
	return a;
}

function pick_rand() {
  var n = new BigInteger('20352011416290410787399145795734689202798264199');
  var n1 = n.subtract(BigInteger.ONE);
  var r = new BigInteger(n.bitLength(), rng);
  var integer = new bigInt(r.mod(n1).add(BigInteger.ONE).toString());
  return integer;
}

$(document).ready(function(){
	
	var a = pick_rand();
	var p = pick_rand();
	var priv1 = pick_rand(); 
	$('#priv1').html(a.toString());
	$('#priv2').html(p.toString());
	$('#pub-y-1').html(priv1.toString());
	
	var mod1 = fastpow(a, priv1, p);
	$('#test1').html(mod1.toString());
	
	//var dataJson = [{'a':a.toString(), 'p':p.toString(), 'mod'.mod1.toString()}]
	
	
	
	$.ajax({
		url: "secret.php",
		type: 'POST',
		data: {'a':a.toString(), 'p':p.toString(), 'mod':mod1.toString()},
		success: function(data)
		{
			data = JSON.parse(data);
			$('#test2').html(data.mod);
			$('#secret2').html(data.secret);
			var mod = new bigInt(data.mod);
			var secret = fastpow(mod, priv1, p);
			$('#secret1').html(secret.toString());
		}
	});

	
//var priv2 = pick_rand();	
	
//	var secret1 = fastpow(mod2, priv1, p);
//	$('#secret1').html(secret1.toString());
	
	
//$('#pub-y-2').html(priv2.toString());	
//	var mod2 = fastpow(a, priv2, p);
//	$('#test2').html(mod2.toString());
//	var secret2 = fastpow(mod1, priv2, p);
//	$('#secret2').html(secret2.toString()); 
	
});